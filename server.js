const express = require("express");
const morgan = require("morgan");
const app = express();
const bodyParser = require("body-parser");
const request = require("request");
const { resolve } = require("path");
const storage = require("node-persist");
const fs = require("fs");
var APPConstants = require("./APPConstants");
var utility = require("./public/utility");
var nodemailer = require("nodemailer");

var interval;

app.use(bodyParser());
app.use(morgan());


app.use(express.static("public"));



app.get("/", async function (request, response) {
  await storage.init();
  var usermaster = await storage.getItem("usermaster");
  if (!usermaster) {
    var userProfiles = await getBody(APPConstants.USER_PROFILE_API);
    var users = await getBody(APPConstants.USER_DATA_API);
    let tempobj = new Object();
    tempobj["userProfiles"] = userProfiles;
    tempobj["users"] = users;
    await storage.setItem("usermaster", JSON.stringify(tempobj));
  }
  response.sendFile(__dirname + "/views/index.html");
});

//redirect to agenotbelow10.html when registered user but above age 10
app.get(APPConstants.AGE_GREATER_THAN_10_URI, (request, response) => {
  response.sendFile(__dirname + "/views/agenotbelow10.html");
});

//redirect to unrecognizeduser.html when he/she is not a regiseterd user
app.get(APPConstants.IVALID_USER_URI, (request, response) => {
  response.sendFile(__dirname + "/views/unrecognizeduser.html");
});

//redirect to success.html when the user is valid and is below 10
app.get("/success", (request, response) => {
  response.sendFile(__dirname + "/views/success.html");
});

app.post(APPConstants.POST_REQUEST_URI, async function (req, res) {
  var usermaster = JSON.parse(await storage.getItem("usermaster"));
  var User = usermaster["users"].filter(
    (user) => user.username === req.body.userid
  );
  if (User && User.length == 1) {
    var profile = usermaster["userProfiles"].filter(
      (profile) => profile.userUid === User[0].uid
    );
    var age = utility._CalculateAge(profile[0].birthdate);
    if (age < 10) {
      jsonReader(APPConstants.SANTA_REMINDER_DATA_FILE, (err, unsentuserdata) => {
        if (err) {
          console.log("Error reading file:", err);
          return;
        }

        unsentuserdata.push({
          username: req.body.userid,
          address: profile[0].address,
          gift: req.body.wish,
          isSent: "No",
        });
        fs.writeFile(
          APPConstants.SANTA_REMINDER_DATA_FILE,
          JSON.stringify(unsentuserdata, null, 2),
          (err) => {
            if (err) {
              console.log("Error writing file:", err);
            } else {
              if(!interval){
              interval = setInterval(sendEmail, 15 * 1000);
              }
              res.redirect("/success");
            }
          }
        );
      });
    } else {
      res.redirect("/agenotbelow10");
    }
  } else {
    res.redirect("/unrecognizeduser");
  }
});

const listener = app.listen(process.env.PORT || 3000, function () {
  console.log("Your app is listening on port " + listener.address().port);
});

async function getBody(url) {
  const options = {
    url: url,
    method: "GET",
  };

  return new Promise(function (resolve, reject) {
    request.get(options, function (err, resp, body) {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
}

//function to read json file, parse and return
function jsonReader(filePath, cb) {
  fs.readFile(filePath, (err, fileData) => {
    if (err) {
      return cb && cb(err);
    }
    try {
      const object = JSON.parse(fileData);
      return cb && cb(null, object);
    } catch (err) {
      return cb && cb(err);
    }
  });
}

function sendEmail() {
  try {
    fs.readFile(APPConstants.SANTA_REMINDER_DATA_FILE, "utf8", function (err, unsentdata) {
      const transporter = nodemailer.createTransport({
        host: APPConstants.EMAIL_HOST,
        port: APPConstants.EMAIL_PORT,
        auth: {
          user: APPConstants.AUTH_USER,
          pass: APPConstants.AUTH_PASSWORD,
        },
      });

      var mailOptions = {
        from: APPConstants.MAIL_FROM,
        to: APPConstants.MAIL_TO,
        subject: APPConstants.MAIL_SUBJECT,
        text: utility._FormEmailMessage(JSON.parse(unsentdata)),
      };
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          fs.writeFile(APPConstants.SANTA_REMINDER_DATA_FILE, JSON.stringify(new Array(), null, 2), (err) => {
            if (err) {
              console.log("Error writing file:", err);
            }else{
              clearInterval(interval);
              interval = false;
            }
          });
          console.log("Email sent: " + info.response);
        }
      });
    });
  } catch (err) {
    console.log(err);
    return;
  }
}
