// define variables that reference elements on our page
const santaForm = document.forms[0];
// listen for the form to be submitted and add a new dream when it is
santaForm.onsubmit = function (event) {

  const userId = document.getElementsByName("userid")[0].value;
  const gift = document.getElementsByName("wish")[0].value;
  
  if(userId.trim() == "") {   
    alert("Did you forget to mention your name ?");
    event.preventDefault();
    return ;
  }
  if (gift.trim() == "") {
    alert("Did you forget to add the wish?");
    event.preventDefault();
    return;
  }
  if (gift.length > 100 ) {
    alert("Sorry but gift should be less than 100 characters");
    event.preventDefault();
    return;
  }
};
