module.exports = {
    _CalculateAge: function (birthday) {
      var datePart = birthday.match(/\d+/g),
        year = datePart[0],
        day = datePart[1],
        month = datePart[2];
      birthday = new Date(year, month - 1, day);
      var ageDifMs = Date.now() - birthday.getTime();
      var ageDate = new Date(ageDifMs);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    },
    _FormEmailMessage: function (jsondata) {
      var message = "Here are the lists of all pending requests.";
      jsondata.forEach((user) => {
        message +=
          "Username: " +
          user.username +
          ", Address: " +
          user.address +
          ", Gift: " +
          user.gift;
          message+="--------------";
      });
      return message;
    },
  };